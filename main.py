from datetime import datetime


class User:
    __slots__ = ("_name", "_age", "_balance", "_interest", "_inventory")

    def __init__(self, name, age, balance=0, interest=()):
        self._name = name
        self._age = age
        self._balance = balance
        self._interest = interest
        self._inventory = []

    def buy_film(self, film):
        if not film._available:
            print("Film is inactive")
        elif self._age < film._age_rating:
            print("Age is less than the age rating.")
        elif self._balance < 0 or film._price < 0:
            print("The balance cannot be negative")
        elif self._balance < film._price:
            print("Insufficient funds on the client's balance sheet")
        else:
            self._balance -= film._price
            self._inventory.append(film)
            print(
                f"Successful purchase of the film: {film._title}.\n"
                f"Schedule: {film._schedule}"
            )

    def film_info(self, film):
        if film._genre in self._interest:
            print(f"The film in the {film._genre} genre is interesting")

        print(film.get_info())

    def __str__(self):
        return f"User: {self._name}\nAge: {self._age}\nBalance: {self._balance}"


class Film:
    __slots__ = (
        "_title",
        "_genre",
        "_rating",
        "_age_rating",
        "_duration",
        "_price",
        "_available",
        "_schedule",
    )

    def __init__(
        self,
        title,
        genre,
        rating,
        age_rating,
        duration=90,
        price=0,
        available=True,
        schedule=(),
    ):
        self._title = title
        self._genre = genre
        self._rating = rating
        self._age_rating = age_rating
        self._duration = duration
        self._price = price
        self._available = available

        if schedule:
            self._schedule = schedule
        else:
            self._schedule = str(datetime.now().date())

    def get_info(self):
        return (
            f"'{self._title}', {self._genre}, {self._rating}, "
            f"{self._age_rating}, {self._duration}, {self._price}, "
            f"{self._available}, {self._schedule}"
        )

    def __str__(self):
        return f"Film: {self._title} | Duration: {self._duration}"


def separator(obj=""):
    separator = "-" * 20

    if obj:
        print(obj, separator, sep="\n")
    else:
        print(separator)


user_1 = User("Michael", 19, 6700, ("Comedy", "Horror"))
user_2 = User("Emil", 14, 1560, ("Action"))
user_3 = User("Eve", 32, 149, ("Phantasy"))

film_1 = Film(
    "Kaze no tani no Naushika", 
    "Phantasy", 
    8, 
    16, 
    117, 
    49
)

film_2 = Film(
    "Oldboy", 
    "Drama", 
    7, 
    18, 
    120, 
    49, 
    False
)

film_3 = Film(
    "Chicken Run: Dawn of the Nugget",
    "Comedy",
    9,
    12,
    90,
    149,
    True,
    ("2024-05-31", "2024-06-31"),
)


print(user_1)
separator(film_1)

separator(film_1.get_info())

user_3.film_info(film_1)
separator()

user_3.buy_film(film_3)
separator(user_3)


user_3.buy_film(film_1)
user_3.buy_film(film_2)

user_test = User("Test", 18, -150)
user_test.buy_film(film_1)
del user_test

user_2.buy_film(film_1)

separator()
